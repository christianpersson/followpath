import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JsonFileParser {
	
	public static void main(String[] args){
		ArrayList<Position> path = parseJsonArrayToPositionList("Path-to-bed.json");
		printPath(path);
	}
		
	public static ArrayList<Position> parseJsonArrayToPositionList(String file) {

		ArrayList<Position> path = new ArrayList<Position>();

		ObjectMapper mapper = new ObjectMapper();
		Collection<Map<String, Object>> jsonArray;
		try {
			jsonArray = mapper.readValue(new File(file), new TypeReference<Collection<Object>>() {
			});
		} catch (IOException e) {
			return null;
		}

		for (Map<String, Object> jsonObject : jsonArray) {
			Map<String, Object> pose = (Map<String, Object>) jsonObject.get("Pose");
			Map<String, Object> orientaiton = (Map<String, Object>) pose.get("Position");
			double x = (double) orientaiton.get("X");
			double y = (double) orientaiton.get("Y");
			path.add(new Position(x, y));
		}
		return path;
	}
	
	public static void printPath(ArrayList<Position> path){
		for(Position pos : path){
			System.out.println(pos.getX() + "\t" + pos.getY());
		}
	}
}
