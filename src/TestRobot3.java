import java.util.ArrayList;

public class TestRobot3 extends TestRobot2 {
	private RobotCommunication robotcomm; // communication drivers

	public TestRobot3(String host, int port) {
		super(host, port);
		robotcomm = new RobotCommunication(host, port);
	}

	public static void main(String[] args) throws Exception {
		System.out.println("Creating Robot");
		TestRobot3 robot = new TestRobot3("http://goblin.cs.umu.se", 50000);

		robot.run();
	}

	public static ArrayList<Position> initPath() {
		ArrayList<Position> path;
		path = new ArrayList<Position>();
		path.add(new Position(0, 0));
		path.add(new Position(0.2, 0));
		path.add(new Position(0.4, 0));
		path.add(new Position(0.6, 0));
		path.add(new Position(0.8, 0));
		path.add(new Position(0.8, 0.1));
		path.add(new Position(0.8, 0.2));
		path.add(new Position(0.8, 0.3));
		path.add(new Position(0.8, 0.4));
		path.add(new Position(0.7, 0.4));
		path.add(new Position(0.6, 0.4));
		path.add(new Position(0.5, 0.4));
		path.add(new Position(0.5, 0.5));
		path.add(new Position(0.5, 0.6));
		path.add(new Position(0.5, 0.7));
		path.add(new Position(0.5, 1.5));



		return path;
	}

	private void run() throws Exception {
		System.out.println("Creating response");
		LocalizationResponse lr = new LocalizationResponse();

		System.out.println("Creating request");
		DifferentialDriveRequest dr = new DifferentialDriveRequest();

		// set up the request to move in a circle
		// dr.setAngularSpeed(Math.PI * 0.25);
		// dr.setLinearSpeed(1.0);

		System.out.println("Start to move robot");
		int rc = robotcomm.putRequest(dr);
		System.out.println("Response code " + rc);

		FollowAlgorithm algorithm = new FollowTheCarrot(JsonFileParser.parseJsonArrayToPositionList("Path-to-bed.json"));

		for (int i = 0; i < 10000; i++) {
//			double start = System.nanoTime();

			// ask the robot about its position and angle
			robotcomm.getResponse(lr);

			double angle = getBearingAngle(lr) / 180 * Math.PI;
//			System.out.println("bearing = " + angle);

			double[] position = getPosition(lr);
//			System.out
//					.println("position = " + position[0] + ", " + position[1]);

			algorithm.setBearingAngle(angle);
			algorithm.setPosition(new Position(position[0], position[1]));

//			System.out.println("Look ahead pos"
//					+ algorithm.getLookAheadPosition());
//			System.out.println("look ahead angle"
//					+ algorithm.getLookAheadAngle());

			double angularSpeed = algorithm.getSteeringAngle();

			System.out.println("Ang. speed: " + angularSpeed);

			dr.setAngularSpeed(angularSpeed/2);
			dr.setLinearSpeed(0.2);
			
			rc = robotcomm.putRequest(dr);
//			System.out.println(System.nanoTime() - start);

		}

		// set up request to stop the robot
		dr.setLinearSpeed(0);
		dr.setAngularSpeed(0);

		System.out.println("Stop robot");
		rc = robotcomm.putRequest(dr);
		System.out.println("Response code " + rc);

	}

	public double sign(double v) {
		if (v > 0)
			return 1.0;
		return -1.0;
	}
}
