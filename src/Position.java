public class Position {
	private double x, y;

	public Position(double pos[]) {
		this.x = pos[0];
		this.y = pos[1];
	}

	public Position(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getDistanceTo(Position p) {
		return Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));
	}

	// bearing relative 'north'
	public double getBearingTo(Position p) {
		return Math.atan2(p.y - y, p.x - x);
	}

	public Position getVectorTo(Position p) {
		return new Position(p.x - this.x, p.y - this.y);
	}

	public double dotProduct(Position p) {
		return p.x * this.x + p.y * this.y;
	}

	public static Position vectorSum(Position p1, Position p2) {
		return new Position(p1.x + p2.x, p1.y + p2.y);
	}

	public Position scale(double scaleFactor) {
		this.x *= scaleFactor;
		this.y *= scaleFactor;
		return this;
	}

	public void transform(Position translation, double rotation) {
		double R11 = Math.cos(rotation), R22 = R11;
		double R12 = -Math.sin(rotation), R21 = -R12;

		double newX = R11 * this.x + R12 * this.y + translation.x;
		double newY = R21 * this.x + R22 * this.y + translation.y;

		this.x = newX;
		this.y = newY;
	}

	public String toString() {
		return "(" + x + ", " + y + ")";
	}

	public Position normalize() {
		return scale(1 / norm());
	}

	public double norm() {
		return Math.sqrt(dotProduct(this));
	}
}
