import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class _JsonFileParserTest {

	@Test
	public void it_returns_null_on_non_existing_file() {
		ArrayList<Position> path = JsonFileParser
				.parseJsonArrayToPositionList("dsds");
		assertNull(path);

	}

	@Test
	public void it_returns_correct_coordinates_on_a_small_array() {
		ArrayList<Position> path = JsonFileParser
				.parseJsonArrayToPositionList("example.json");
		assertEquals(2, path.size());

		Position p = path.get(0);
		assertEquals(1, p.getX(), 0.001);
		assertEquals(4, p.getY(), 0.001);
	}
}
