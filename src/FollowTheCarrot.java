import java.util.ArrayList;

public class FollowTheCarrot extends FollowAlgorithm {

	public FollowTheCarrot(ArrayList<Position> path) {
		this.path = path;
	}

	public double getLookAheadAngle() {
		return p.getBearingTo(getLookAheadPosition());
	}
	
	public double getSteeringAngle() {
		double theta = getBearingAngle();
		double phi = getLookAheadAngle();
		double epsilon = (phi - theta) * GAIN;
		return Calculation.getAngleIn0Branch(epsilon);
	}
}
