public class Calculation {

	public static double distance(Position p, Position p0, Position p1) {
		Position v = p0.getVectorTo(p1);
		Position w = p0.getVectorTo(p);

		double c1, c2;
		if ((c1 = w.dotProduct(v)) <= 0) {
			return p.getDistanceTo(p0); // left of P0
		}

		if ((c2 = v.dotProduct(v)) <= c1) {
			return p.getDistanceTo(p1); // right of P1
		}

		double b = c1 / c2;

		Position Pb = Position.vectorSum(p0, v.scale(b));

		return p.getDistanceTo(Pb);

	}

	public static double getAngleIn0Branch(double angle) {
		if (angle > Math.PI) {
			return angle - 2 * Math.PI;
		} else if (angle < -Math.PI) {
			return angle + 2 * Math.PI;
		}
		return angle;
	}

	
}
