import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class _FollowCarrotTest {

	private static final double ERROR_TOL = 0.01;
	public ArrayList<Position> path;

	public static ArrayList<Position> initPath() {
		ArrayList<Position> path;
		path = new ArrayList<Position>();
		path.add(new Position(1, 1));
		path.add(new Position(4, 1));
		path.add(new Position(5, 3));
		path.add(new Position(3, 4));
		path.add(new Position(2, 3));
		return path;
	}

	@Test
	public void find_start_point_from_start_position() {
		path = initPath();
		FollowAlgorithm algorithm = new FollowTheCarrot(path);
		algorithm.setPosition(new Position(0, 0));

		assertEqualPositions(new Position(4, 1),
				algorithm.getLookAheadPosition());

	}

	@Test
	public void find_path_point_from_random_point() {
		initPath();
		FollowAlgorithm algorithm = new FollowTheCarrot(path);
		algorithm.setPosition(new Position(5, 2));
		assertEqualPositions(new Position(5, 3),
				algorithm.getLookAheadPosition());
	}

	@Test
	public void find_path_point_on_a_segment_pont() {
		initPath();
		FollowAlgorithm algorithm = new FollowTheCarrot(path);
		algorithm.setPosition(new Position(5, 3));
		assertEqualPositions(new Position(3, 4),
				algorithm.getLookAheadPosition());
	}

	@Test
	public void find_path_point_when_passed_end_point() {
		initPath();
		FollowAlgorithm algorithm = new FollowTheCarrot(path);
		algorithm.setPosition(new Position(1, 3));
		assertEqualPositions(new Position(2, 3),
				algorithm.getLookAheadPosition());
	}

	public static void assertEqualPositions(Position expected, Position actual) {
		assertEquals(expected.getX(), actual.getX(), ERROR_TOL);
		assertEquals(expected.getY(), actual.getY(), ERROR_TOL);
	}

}
