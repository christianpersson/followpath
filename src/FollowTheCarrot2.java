import java.util.ArrayList;

public class FollowTheCarrot2 extends FollowAlgorithm {

	private static final double LOOK_AHEAD_DISTANCE = 0.2;

	public FollowTheCarrot2(ArrayList<Position> path) {
		this.path = path;
	}

	public double getLookAheadAngle() {
		Position lookAhead = getLookAheadPosition().normalize().scale(LOOK_AHEAD_DISTANCE);
		setPosition(lookAhead);
		Position newLookAhead = getLookAheadPosition();
		return p.getBearingTo(newLookAhead);
	}
	
	
	
	public double getSteeringAngle() {
		double theta = getBearingAngle();
		double phi = getLookAheadAngle();
		double epsilon = (phi - theta) * GAIN;
		return Calculation.getAngleIn0Branch(epsilon);
	}
}
