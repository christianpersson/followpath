import java.util.ArrayList;

public abstract class FollowAlgorithm {

	static final int GAIN = 1;
	
	protected ArrayList<Position> path;
	protected Position p;
	protected double bearingAngle;
	
	public abstract double getLookAheadAngle();

	public FollowAlgorithm() {
		super();
	}

	public double getBearingAngle() {
		return bearingAngle;
	}

	public void setBearingAngle(double angle) {
		 bearingAngle = angle;
	}

	public void setPosition(Position pos) {
		this.p = pos;
	}

	public Position getLookAheadPosition() {
		Position p0 = null, lookAheadPosition = null;
		double minDistance = 101001011000.0, distance;
		for (Position p1 : path) {
			if (p0 != null) {
				distance = Calculation.distance(p, p0, p1);
				if (distance <= minDistance) {
					lookAheadPosition = p1;
					minDistance = distance;
				}
			}
			p0 = p1;
		}
		return lookAheadPosition;
	}

	public abstract double getSteeringAngle();
	



}